- See the background white paper
<a href="https://docs.google.com/document/d/1lEkiNrbXQVM3xj4M7dQgHE0xSix-G0Q2XCo-TH7Yum4/edit?usp=sharing">Technological autonomy not autonomous technology</a>

- <a href="https://opengrains.cc/"> OpenGrains video on the emergence of grassroots agricultural innovation networks</a>


- <a href="https://www.csm4cfs.org/csipm-vision-statement-on-data-for-food-security-and-nutrition"> CSIPM Vision Statement on Data for Food security and Nutrition </a>


- <a href="
https://us06web.zoom.us/rec/share/dTmOzI6Mwcjt_XKM0jHjtH7UhowIXS5rQsZTlKjQNpgmt16LPtzk5Br0gy8yEGOC.IxEAczwpRXA7an88"> September 28th Grassroots Innovations for Agroecology Meeting</a> 
<br> Passcode: c5k%@NYm
<br>
<i> This session included a series of short presentations of grassroots experiences of agroecology innovations and processes followed by an an open dialogue with researchers and representatives from these organizations. </i>